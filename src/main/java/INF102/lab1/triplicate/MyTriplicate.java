package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public <T extends Comparable> T findTriplicate(List<T> list) {
        Collections.sort(list);
        for(int i = 0; i < list.size()-2; i++){
            T val_of_index = list.get(i);
            T val_of_index2 = list.get(i+1);
            T val_of_index3 = list.get(i+2);
            
            if(val_of_index.equals(val_of_index2) && val_of_index.equals(val_of_index3)){
                return val_of_index;
            }
        }
        return null;
    }
    
}
